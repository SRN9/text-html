<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>MTea</title>
</head>
<style>
    .box {
        margin: 80px auto;
        width: 600px;
        height: 450px;
        background-color: lightgrey;
        background: rgba(255, 255, 255, 0.6);
    }

    form {
        margin: 50px 0;
    }

    form label {
        line-height: 50px;
    }
</style>
<script>

</script>
<body>

<div class="box">
    <form action="updateTeacherServlet" method="post" align="center">
        <label for="tea_id">学号:</label>
        <input type="text" value="${Tea.tea_id}" name="tea_id" id="tea_id">
        <p></p>
        <label for="tea_name">姓名:</label>
        <input type="text" value="${Tea.tea_name}" name="tea_name" id="tea_name">
        <p></p>
        <label for="gender">性别:</label>
        <input type="text" value="${Tea.gender}" name="gender" id="gender">
        <p></p>
        <label for="grade">年级:</label>
        <input type="text" value="${Tea.grade}" name="grade" id="grade">
        <p></p>
        <label for="phoneNumber">电话:</label>
        <input type="text" value="${Tea.phoneNumber}" name="phoneNumber" id="phoneNumber">
        <p></p>
        <label for="password">登录密码:</label>
        <input type="text" value="${Tea.password}" name="password" id="password">
        <p></p>
        <button type="submit">确认修改</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </form>
</div>

</body>
</html>

