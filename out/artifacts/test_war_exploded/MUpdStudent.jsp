<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>MUpdStudent</title>
</head>
<style>
    .box {
        margin: 80px auto;
        width: 600px;
        height: 450px;
        background-color: lightgrey;
        background: rgba(255, 255, 255, 0.6);
    }

    form {
        margin: 50px 0;
    }

    form label {
        line-height: 50px;
    }
</style>
<body>

<div class="box">
    <form action="/updateStudentServlet" method="post" align="center">
        <label for="stu_id">学号:</label>
        <input type="text" value="${stu.stu_id}" name="stu_id" id="stu_id">
        <p></p>
        <label for="stu_name">姓名:</label>
        <input type="text" value="${stu.stu_name}" name="stu_name" id="stu_name">
        <p></p>
        <label for="gender">性&nbsp;&nbsp;别:</label>
        <c:if test="${stu.gender == '男'}">
            &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio"  name="gender" id="gender" value="男" checked />&nbsp;&nbsp;男
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio"  name="gender" id="gender" value="女" />&nbsp;&nbsp;女
        </c:if>
        <c:if test="${stu.gender == '女'}">
            &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio"  name="gender" id="gender" value="男"  />&nbsp;&nbsp;男
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio"  name="gender" id="gender" value="女" checked/>&nbsp;&nbsp;女
        </c:if>
        <p></p>
        <p></p>
        <label for="grade">年级:</label>
        <input type="text" value="${stu.grade}" name="grade" id="grade">
        <p></p>
        <label for="phoneNumber">电话:</label>
        <input type="text" value="${stu.phoneNumber}" name="phoneNumber" id="phoneNumber">
        <p></p>
        <label for="password">登录密码:</label>
        <input type="text" value="${stu.password}" name="password" id="password">
        <p></p>
        <button type="submit">提交</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="/mStudentServlet"><input type="button" value="返回"></a>
    </form>
</div>

</body>
</html>

