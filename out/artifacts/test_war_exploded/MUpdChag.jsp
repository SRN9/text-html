<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>Title</title>
</head>
<style>
    .box {
        margin: 80px auto;
        width: 600px;
        height: 450px;
        background-color: lightgrey;
        background: rgba(255, 255, 255, 0.6);
    }

    form {
        margin: 50px 0;
    }

    form label {
        line-height: 50px;
    }
</style>
<body>

<div class="box">
    <form action="/updateChagServlet" method="post" align="center">
        <label for="idA">A学号:</label>
        <input type="text" value="${list.idA}" name="idA" id="idA">
        <p></p>
        <label for="seatA">A座位编号:</label>
        <input type="text" value="${list.seatA}" name="seatA" id="seatA">
        <p></p>
        <label for="idB">B学号:</label>
        <input type="text" value="${list.idB}" name="idB" id="idB">
        <p></p>
        <label for="seatB">B座位编号:</label>
        <input type="text" value="${list.seatB}" name="seatB" id="seatB">
        <p></p>
        <label for="approval">申请状态:</label>
        <c:if test="${list.approval == '-1'}">
            <input type="radio"  name="approval" id="approval" value="-1" checked />失败
            <input type="radio"  name="approval" id="approval" value="0" />待审核
            <input type="radio"  name="approval" id="approval" value="1" />成功
        </c:if>
        <c:if test="${list.approval == '0'}">
            <input type="radio"  name="approval" id="approval" value="-1" />失败
            <input type="radio"  name="approval" id="approval" value="0" checked  />待审核
            <input type="radio"  name="approval" id="approval" value="1" />成功
        </c:if>
        <c:if test="${list.approval == '1'}">
            <input type="radio"  name="approval" id="approval" value="-1"  />失败
            <input type="radio"  name="approval" id="approval" value="0" />待审核
            <input type="radio"  name="approval" id="approval" value="1" checked/>成功
        </c:if>
        <p></p>
        <button type="submit">提交</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="/mChangeServlet"><input type="button" value="返回"></a>
    </form>
</div>

</body>
</html>


