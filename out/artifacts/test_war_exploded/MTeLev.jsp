<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>Title</title>
</head>
<style>
    .box {
        margin: 80px auto;
        width: 600px;
        height: 450px;
        background-color: lightgrey;
        background: rgba(255, 255, 255, 0.6);
    }

    form {
        margin: 50px 0;
    }

    form label {
        line-height: 50px;
    }
</style>
<body>

<div class="box">
    <form action="/updateLevServlet" method="post" align="center">
        <label for="stu_id">学号:</label>
        <input type="text" value="${les.stu_id}" name="stu_id" id="stu_id">
        <p></p>
        <label for="seatId">座位编号:</label>
        <input type="text" value="${les.seatId}" name="seatId" id="seatId">
        <p></p>
        <label for="timeStart">起始时间:</label>
        <input type="text" value="${les.timeStart}" name="timeStart" id="timeStart">
        <p></p>
        <label for="timeEnd">截止时间:</label>
        <input type="text" value="${les.timeEnd}" name="timeEnd" id="timeEnd">
        <p></p>
        <label for="days">请假天数:</label>
        <input type="text" value="${les.days}" name="days" id="days">
        <p></p>
        <label for="approval">申请状态:</label>
        <c:if test="${les.approval == '-1'}">
            <input type="radio"  name="approval" id="approval" value="-1" checked />失败
            <input type="radio"  name="approval" id="approval" value="0" />待审核
            <input type="radio"  name="approval" id="approval" value="1" />成功
        </c:if>
        <c:if test="${les.approval == '0'}">
            <input type="radio"  name="approval" id="approval" value="-1" />失败
            <input type="radio"  name="approval" id="approval" value="0" checked  />待审核
            <input type="radio"  name="approval" id="approval" value="1" />成功
        </c:if>
        <c:if test="${les.approval == '1'}">
            <input type="radio"  name="approval" id="approval" value="-1"  />失败
            <input type="radio"  name="approval" id="approval" value="0" />待审核
            <input type="radio"  name="approval" id="approval" value="1" checked/>成功
        </c:if>
        <p></p>
        <button type="submit">提交</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="/mTeaLeaServlet"><input type="button" value="返回"></a>
    </form>
</div>

</body>
</html>


