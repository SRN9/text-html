<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<style>

    .table {
        margin: 60px 120px;
    }

    .table table td {
        border-top: 1px solid lightblue;
    }



</style><%--
<script>
  function deleteSeat(seatId){
        if(confirm("您确定要删除吗？")){
            location.href="${pageContext.request.contextPath}/delSeatServlet?seatId="+seatId;
        }
    }
</script>--%>
<head>
    <title>MClassroom</title>
</head>
<body>
<form class="table" action="mClassroomServlet" method="post">
    <label for="roomId">教室查询：教室号:</label>
    <input type="text" name="roomId" id="roomId">
    <button type="submit">查询</button>

<div style="height:400px;width:800px;display:block;overflow-y:auto;margin:30px auto;  ">


    <table  width="100%" cellspacing="0" cellpadding="10" align="center">
        <tr background="../image/login_bg.png" height="44px" >
            <th>教室号</th>
            <th>座位号</th>
            <th>座位编号</th>
            <th>操作处理</th>
        </tr>

        <c:forEach items="${seats}" var="seat">
            <tr bgcolor="#e0ffff" align="center" height ="20px">
                <td>${seat.roomId}</td>
                <td>${seat.seatNumber}</td>
                <td>${seat.seatId}</td>
                    <%--  <td><a class="button" href="javascript:deleteSeat(${seat.seatId});">删除</a></td>--%>
            <td><a class="button" href="/delSeatServlet?seatId=${seat.seatId}">删除</a></td>
        </tr>
    </c:forEach>

    <tr bgcolor="#f5f5f5" align="center" height ="20px">
        <td colspan="4"><a href="MAddSeat.jsp">添加座位</a></td>
    </tr>
</table></div>
</form>
</body>
</html>
