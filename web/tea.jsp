<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>teacher_login_success</title>
</head>
<body>

    <link rel="stylesheet" type="text/css" href="css/manager.css"/>

<div id="box">
    <div id="head">
        <div class="logo">
            <font face="宋体" color="black" style="font-size: 15pt">考研教室管理系统</font>
            <div style="text-align:right;font-size:9pt;color:darkorange">
                <strong>${pageContext.session.getAttribute("teaName")}</strong> <span>,欢迎登录！</span>
                <span><a href="index.jsp" target="_parent">退出</a></span>
            </div>
        </div>
        <div class="menu">
            <ul>
                <li><a href="tea.jsp">首页</a></li>
                <li>
                    <a href="tea.jsp">信息管理</a>
                    <ul>

                       <li><a href="/findTeacherServlet?Tea_id=${pageContext.session.getAttribute("teaId")}" target="select-frame">个人信息</a></li>
                        <li><a href="mTeaStuServlet" target="select-frame">学生信息</a></li>
                    </ul>
                </li>


                <li>
                    <a href="#">审核处理</a>
                    <ul>
                        <li><a href="mTeaDistriServlet" target="select-frame">申请名单</a></li>
                        <li><a href="mTeaLeaServlet" target="select-frame">请假信息</a></li>

                    </ul>
                </li>

            </ul>
        </div>
    </div>
    <iframe id="body" name="select-frame" width="100%" height="100%" frameborder="0" scrolling="no" class="center">

    </iframe>

</div>
</body>
</html>

</body>
</html>
