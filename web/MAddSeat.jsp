<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MAddSeat</title>
</head>
<style>
    .box {
        margin: 80px auto;
        width: 600px;
        height: 250px;
        background-color: lightgrey;
        background: rgba(255, 255, 255, 0.6);
    }

    form {
        margin: 50px 0;
    }

    form label {
        line-height: 50px;
    }
</style>
<body>
<div class="box">
    <form action="/addSeatServlet" method="post" align="center">
        <label for="seatId">座位编号:</label>
        <input type="text" placeholder="请输入座位编号" name="seatId" id="seatId">
        <p></p>
        <label for="roomId">教&nbsp;室&nbsp;号:</label>
        <input type="text" placeholder="请输入教室号" name="roomId" id="roomId">
        <p></p>
        <label for="seatNumber">座&nbsp;位&nbsp;号:</label>
        <input type="text" placeholder="请输入座位号" name="seatNumber" id="seatNumber">
        <p></p>
        <button type="submit">提交</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="/mClassroomServlet"><input type="button" value="返回"></a>
    </form>
</div>
</body>
</html>
