<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MAddStudent</title>
</head>
<style>
    .box {
        margin: 80px auto;
        width: 600px;
        height: 450px;
        background-color: lightgrey;
        background: rgba(255, 255, 255, 0.6);
    }

    form {
        margin: 50px 0;
    }

    form label {
        line-height: 50px;
    }
</style>
<body>

<div class="box">
    <form action="/addStudentServlet" method="post" align="center">
        <label for="stu_id">学号:</label>
        <input type="text" placeholder="请输入学号" name="stu_id" id="stu_id">
        <p></p>
        <label for="stu_name">姓名:</label>
        <input type="text" placeholder="请输入姓名" name="stu_name" id="stu_name">
        <p></p>
        <label for="gender">性别:</label>
        <input type="text" placeholder="请输入性别" name="gender" id="gender">
        <p></p>
        <label for="grade">年级:</label>
        <input type="text" placeholder="请输入年级" name="grade" id="grade">
        <p></p>
        <label for="phoneNumber">电话:</label>
        <input type="text" placeholder="请输入电话" name="phoneNumber" id="phoneNumber">
        <p></p>
        <label for="password">登录密码:</label>
        <input type="text" placeholder="请输入登录密码" name="password" id="password">
        <p></p>
        <button type="submit">提交</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="/mStudentServlet"><input type="button" value="返回"></a>
    </form>
</div>

</body>
</html>
