<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>manager_login_success</title>
</head>
<body>
    <link rel="stylesheet" type="text/css" href="css/manager.css"/>
<div id="box">
    <div id="head">
        <div class="logo">
            <font face="宋体" color="black" style="font-size: 15pt">考研教室管理系统</font>
            <div style="text-align:right;font-size:9pt;color:darkorange">
                <strong>${pageContext.session.getAttribute("magName")}</strong> <span>,欢迎登录！</span>
                <span><a href="index.jsp" target="_parent">退出</a></span>
            </div>
        </div>
        <div class="menu">
            <ul>
                <li><a href="mag.jsp">首页</a></li>
                <li>
                <a href="mag.jsp">信息管理</a>
                <ul>
                    <li><a href="mClassroomServlet" target="select-frame">教室信息</a></li>
                    <li><a href="mStudentServlet" target="select-frame">学生信息</a></li>
                </ul>
                </li>


                <li>
                    <a href="#">审核处理</a>
                    <ul>
                        <li><a href="mDistriServlet" target="select-frame">分配名单</a></li>
                        <li><a href="mLeavServlet" target="select-frame">请假信息</a></li>
                        <li><a href="mChangeServlet" target="select-frame">换座审核</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
    <iframe id="body" name="select-frame" width="100%" height="100%" frameborder="0" scrolling="no" class="center">

    </iframe>

</div>
</body>
</html>

</body>
</html>
