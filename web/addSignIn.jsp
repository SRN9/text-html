<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>AddSignIn</title>
</head>
<style>
    .head {
        margin:40px 80px;
        float:left;
        width:800px;
        height:800px;
        clear:right;

    }
</style>
<body>
<span><strong>报名申请:</strong></span>
<form action="/signInServlet" method="post" class="head">
    <label for="stu_id">学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;号:</label>
    <input type="text" value="${pageContext.session.getAttribute("stuId")}" name="stu_id" id="stu_id" readonly="readonly">
    <label for="stu_name">姓名:</label>
    <input type="text" placeholder="请输入姓名" name="stu_name" id="stu_name">
    <label for="grade">班级:</label>
    <input type="text" placeholder="请输入年级" name="grade" id="grade"><p></p>
    <label for="phoneNumber">联系电话:</label>
    <input type="text" placeholder="请输入联系电话" name="phoneNumber" id="phoneNumber"><p></p>
    <button type="submit">提交</button>
    <a href="/stu.jsp"><input type="button" value="返回首页"></a><p></p>
    <strong>${msg}</strong>
</form>

</body>
</html>
