<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>MLea</title>
</head>
<style>

    .table {
        margin: 60px 120px;
    }

    .table table td {
        border-top: 1px solid lightblue;
    }


</style>
<body>
<form class="table" action="mLeavServlet" method="post">
    <label for="stu_id">查询方式一：学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;号:</label>
    <input type="text" name="stu_id" id="stu_id">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <label for="seatId">查询方式二：座位编号:</label>
    <input type="text" name="seatId" id="seatId">
    <p></p>
    <label for="timeStart">查询方式三：起始时间:</label>
    <input type="date" name="timeStart" id="timeStart">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <label for="timeEnd">查询方式四：截止时间:</label>
    <input type="date" name="timeStart" id="timeEnd">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <button type="submit">查询</button>
    <div style="height:400px;width:800px;display:block;overflow-y:auto;margin:30px auto; ">
        <table width="100%" cellspacing="0" cellpadding="10" align="center">
            <tr background="../image/login_bg.png" height="44px">
                <th>学号</th>
                <th>座位编号</th>
                <th>起始时间</th>
                <th>截止时间</th>
                <th>请假天数</th>
                <th>申请状态</th>
                <th colspan="2">操作处理</th>
            </tr>

            <c:forEach items="${les}" var="les">
                <tr bgcolor="#e0ffff" align="center" height="20px">
                    <td>${les.stu_id}</td>
                    <td>${les.seatId}</td>
                    <td>${les.timeStart}</td>
                    <td>${les.timeEnd}</td>
                    <td>${les.days}</td>
                    <td>
                        <c:if test="${les.approval == '1'}">
                            成功
                        </c:if>
                        <c:if test="${les.approval == '0'}">
                            待审核
                        </c:if>
                        <c:if test="${les.approval == '-1'}">
                            失败
                        </c:if>



                    </td>
                    <td align="left"><a class="button" href="/findTeaLevServlet?stu_id=${les.stu_id}">修改</a></td>
                    <td align="left"><a class="button" href="/delServlet?stu_id=${les.stu_id}">删除</a></td>
                </tr>
            </c:forEach>


        </table>
    </div>
</form>
</body>
</html>
