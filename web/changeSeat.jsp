<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>changSeat</title>
</head>
<style>
    .head {
        margin:40px 80px;
        float:left;
        width:800px;
        height:800px;
        clear:right;

    }
</style>
<body>
<span><strong>换座申请:</strong></span>
<form action="/changeSeatServlet" method="post" class="head">
    <label for="stu_id">我的学号:</label>
    <input type="text" value="${pageContext.session.getAttribute("stuId")}" name="stu_id" id="stu_id" readonly="readonly">
    <label for="seatId">交换座位号:</label>
    <input type="text" placeholder="请输入座位编号" name="seatId" id="seatId"><p></p>
    <label for="id">用&nbsp;&nbsp;户&nbsp;&nbsp;名:</label>
    <input type="text" placeholder="请输入对方用户名以确认" name="id" id="id">
    <label for="password">密码:</label>
    <input type="password" placeholder="请输入对方密码以确认" name="password" id="password"><p></p>
    <button type="submit">提交</button>
    <a href="/stu.jsp"><input type="button" value="返回首页"></a><p></p>
    <strong>${msg}</strong></form>

</body>
</html>
