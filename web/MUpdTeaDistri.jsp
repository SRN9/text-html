<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <head>
        <title>MUpdDistri</title>
    </head>
    <style>
        .box {
            margin: 80px auto;
            width: 600px;
            height: 450px;
            background-color: lightgrey;
            background: rgba(255, 255, 255, 0.6);
        }

        form {
            margin: 50px 0;
        }

        form label {
            line-height: 50px;
        }
    </style>
<body>

<div class="box">
    <form action="/updateDistriServlet" method="post" align="center">
        <label for="askNumber">编号:</label>
        <input type="text" readonly value="${list.askNumber}" name="askNumber" id="askNumber">
        <p></p>
        <label for="stu_id">学号:</label>
        <input type="text" value="${list.stu_id}" name="stu_id" id="stu_id">
        <p></p>
        <label for="seatId">座位号:</label>
        <input type="text" readonly value="${list.seatId}" name="seatId" id="seatId">
        <p></p>
        <label for="askStatus">申请状态:</label>
        <c:if test="${list.askStatus == '-1'}">
            <input type="radio"  name="askStatus" id="askStatus" value="-1" checked />失败
            <input type="radio"  name="askStatus" id="askStatus" value="1" />成功
            <input type="radio"  name="askStatus" id="askStatus" value="0" />待审核
        </c:if>
        <c:if test="${list.askStatus == '1'}">
            <input type="radio"  name="askStatus" id="askStatus" value="-1"  />失败
            <input type="radio"  name="askStatus" id="askStatus" value="1" checked/>成功
            <input type="radio"  name="askStatus" id="askStatus" value="0" />待审核
        </c:if>
        <c:if test="${list.askStatus == '0'}">
            <input type="radio"  name="askStatus" id="askStatus" value="-1"  />失败
            <input type="radio"  name="askStatus" id="askStatus" value="1" />成功
            <input type="radio"  name="askStatus" id="askStatus" value="0" checked/>待审核
        </c:if>
        <c:if test="${list.askStatus == '2'}">

            <input type="radio"  name="askStatus" id="askStatus" value="2" checked/>有座（无权限修改）
        </c:if>

        <p></p>
        <button type="submit">提交</button>
        <a href="/mTeaDistriServlet"><input type="button" value="返回"></a>
    </form>
</div>

</body>
</html>

