<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>listSearch</title>
</head>
<style>
    .head {
        margin: 40px 60px;
        float: left;
        width: 800px;
        height: 800px;
        clear: right;

    }

    table td {
        border-top: 1px solid lightblue;
    }
</style>
<body>
<span><strong>名单查询:</strong></span>
<form action="/listSearchServlet" method="post" class="head">
    <label for="stu_id">查询方式一：学&nbsp;&nbsp;&nbsp;号:</label>
    <input type="text" name="stu_id" id="stu_id">
    <p></p>
    <label for="roomId">查询方式二：教室号:</label>
    <input type="text" name="roomId" id="roomId">
    <label for="seatNumber">座位号:</label>
    <input type="text" name="seatNumber" id="seatNumber">
    <p></p>
    <button type="submit">查询</button>
    <a href="/stu.jsp"><input type="button" value="返回首页"></a>

    <div style="height:400px;width:500px;display:block;overflow-y:auto;margin:10px 10px; ">
        <table width="100%" cellspacing="0" cellpadding="10" align="center">
            <tr background="../image/login_bg.png" height="44px">
                <th>学号</th>
                <th>教室号</th>
                <th>座位号</th>
            </tr>
            <c:forEach items="${list}" var="list">
                <c:if test="${list.askStatus eq 2}">
                    <tr bgcolor="#e0ffff" align="center" height="20px">
                        <td>${list.stu_id}</td>
                        <td>${fn:substring(list.seatId,0,2)}</td>
                        <td>${fn:substring(list.seatId,2,4)}</td>
                    </tr>
                </c:if>
            </c:forEach>
        </table>
    </div>
</form>
</body>
</html>
