<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>MDistribution</title>
</head>
<style>

    .table {
        margin: 60px 120px;
    }

    .table table td {
        border-top: 1px solid lightblue;
    }


</style>
<body>

<form class="table" action="mDistriServlet" method="post">
    <label for="stu_id">查询方式一：学&nbsp;&nbsp;&nbsp;号:</label>
    <input type="text" name="stu_id" id="stu_id">
    <p></p>
    <label for="roomId">查询方式二：教室号:</label>
    <input type="text" name="roomId" id="roomId">
    <label for="seatNumber">座位号:</label>
    <input type="text" name="seatNumber" id="seatNumber">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <button type="submit">查询</button>
    <div style="height:400px;width:800px;display:block;overflow-y:auto;margin:30px auto; ">
        <table width="100%" cellspacing="0" cellpadding="10" align="center">
            <tr background="../image/login_bg.png" height="44px">
                <th>编号</th>
                <th>学号</th>
                <th>座位号</th>
                <th>申请状态</th>
                <th colspan="2">操作处理</th>
            </tr>

            <c:forEach items="${lists}" var="list">
                <tr bgcolor="#e0ffff" align="center" height="20px">
                    <td>${list.askNumber}</td>
                    <td>${list.stu_id}</td>
                    <td>${list.seatId}</td>

                    <td>
                        <c:if test="${list.askStatus == '-1'}">
                            无座
                        </c:if>

                        <c:if test="${list.askStatus == '1'}">
                            候补
                        </c:if>
                        <c:if test="${list.askStatus == '2'}">
                            有座
                        </c:if>
                    </td>
                    <td align="right"><a class="button" href="/findListServlet?stu_id=${list.stu_id}">修改</a></td>
                    <td align="left"><a class="button" href="/delDistriServlet?stu_id=${list.stu_id}">删除</a></td>
                </tr>
            </c:forEach>


        </table>
    </div>
</form>

</body>
</html>
