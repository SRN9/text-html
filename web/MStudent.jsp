<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>MStudent</title>
</head>
<style>

    .table {
        margin: 60px 120px;
    }

    .table table td {
        border-top: 1px solid lightblue;
    }
</style>
<body>
<form class="table" action="mStudentServlet" method="post">
    <label for="stu_id">查询方式一：学&nbsp;&nbsp;&nbsp;号:</label>
    <input type="text" name="stu_id" id="stu_id">
    <p></p>
    <label for="grade">查询方式二：班&nbsp;&nbsp;&nbsp;级:</label>
    <input type="text" name="grade" id="grade">
    <button type="submit">查询</button>
    <div style="height:400px;width:800px;display:block;overflow-y:auto;margin:30px auto; ">
        <table  width="100%" cellspacing="0" cellpadding="10" align="center">
            <tr background="../image/login_bg.png" height="44px" >
                <th>学号</th>
                <th>姓名</th>
                <th>性别</th>
                <th>班级</th>
                <th>电话号码</th>
                <th>登录密码</th>
                <th colspan="2">操作处理</th>
            </tr>

            <c:forEach items="${students}" var="student">
                <tr bgcolor="#e0ffff" align="center" height ="20px">
                    <td>${student.stu_id}</td>
                    <td>${student.stu_name}</td>
                    <td>${student.gender}</td>
                    <td>${student.grade}</td>
                    <td>${student.phoneNumber}</td>
                    <td>${student.password}</td>
                    <td align="right"><a class="button" href="/findStudentServlet?stu_id=${student.stu_id}">修改</a></td>
                    <td align="left"><a class="button" href="/delStudentServlet?stu_id=${student.stu_id}">删除</a></td>
                </tr>
            </c:forEach>
            <tr bgcolor="#f5f5f5" align="center" height ="20px">
                <td colspan="8"><a href="MAddStudent.jsp">添加学生用户</a></td>
            </tr>


        </table>
    </form></div>
</body>
</html>
