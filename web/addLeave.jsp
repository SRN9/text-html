<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>addLeave</title>
</head>
<style>
    .head {
        margin:40px;
        float:left;
        width:800px;
        height:800px;
        clear:right;

    }

</style>
<body>
    <span><strong>请假申请:</strong></span>

<form action="/askForLeaveServlet" method="post" class="head">
    <label for="stu_id">学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;号:</label>
    <input type="text" value="${pageContext.session.getAttribute("stuId")}" name="stu_id" id="stu_id" readonly="readonly">
    <label for="seatId">座位编号:</label>
    <input type="text" placeholder="请输入座位编号" name="seatId" id="seatId"><p></p>
    <label for="timeStart">开始时间:</label>
    <input type="date" name="timeStart" id="timeStart">
    <label for="timeEnd">结束时间:</label>
    <input type="date"  name="timeEnd" id="timeEnd">
    <label for="days">请假天数:</label>
    <input type="text" placeholder="请输入请假天数" name="days" id="days"><p></p>
    <button type="submit">提交</button>
    <a href="/stu.jsp"><input type="button" value="返回首页"></a>
</form>
<c:if test="${leave.approval == 0}">
    <strong>请假审批中，请等候</strong>
</c:if>
<c:if test="${leave.approval == 1}">
    <strong>请假成功,旅途愉快！</strong>
</c:if>
<c:if test="${leave.approval == -1}">
    <strong>请假未审核通过！</strong>
</c:if>
</body>
</html>
