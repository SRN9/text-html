<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>MChag</title>
</head>
<style>

    .table {
        margin: 60px 120px;
    }

    .table table td {
        border-top: 1px solid lightblue;
    }


</style>
<body>

<form class="table">
    
    <div style="height:400px;width:800px;display:block;overflow-y:auto;margin:30px auto; ">
        <table width="100%" cellspacing="0" cellpadding="10" align="center">
            <tr background="../image/login_bg.png" height="44px">
                <th>A学号</th>
                <th>A座位编号</th>
                <th>B学号</th>
                <th>B座位编号</th>
                <th>申请状态</th>
                <th colspan="2">操作处理</th>
            </tr>

            <c:forEach items="${chags}" var="chags">
                <tr bgcolor="#e0ffff" align="center" height="20px">
                    <td>${chags.idA}</td>
                    <td>${chags.seatA}</td>
                    <td>${chags.idB}</td>
                    <td>${chags.seatB}</td>
                    <td>
                        <c:if test="${chags.approval == '1'}">
                            成功
                        </c:if>
                        <c:if test="${chags.approval == '0'}">
                            待审核
                        </c:if>
                        <c:if test="${chags.approval == '-1'}">
                            失败
                        </c:if>


                    </td>

                    <td align="right"><a class="button" href="/findChagServlet?idA=${chags.idA}">修改</a>
                    <td align="left"><a class="button" href="/delChagServlet?idA=${chags.idA}">删除</a></td>
                </tr>
            </c:forEach>


        </table>
    </div>
</form>



</body>
</html>
