package dao.daoImp;

import dao.Dao;
import domain.*;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import utils.JDBCUtils;

import java.util.List;

public class DaoImp implements Dao {
    JdbcTemplate template = new JdbcTemplate(JDBCUtils.getDataSource());
    @Override
    public Student stuLogin(Student stu) {
        try {
            String sql = "select * from student where stu_id = ? and password = ?";
            return template.queryForObject(sql, new BeanPropertyRowMapper<Student>(Student.class), stu.getStu_id(), stu.getPassword());
        } catch (DataAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Teacher teaLogin(Teacher tea) {
        try {
            String sql = "select * from teacher where tea_id = ? and password = ?";
            return template.queryForObject(sql, new BeanPropertyRowMapper<Teacher>(Teacher.class), tea.getTea_id(), tea.getPassword());
        } catch (DataAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Manager magLogin(Manager mag) {
        try {
            String sql = "select * from manage where mag_id = ? and password = ?";
            return template.queryForObject(sql, new BeanPropertyRowMapper<Manager>(Manager.class), mag.getMag_id(), mag.getPassword());
        } catch (DataAccessException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public List<Seat> findAllRoom() {
        String sql = "select * from seat";
        List<Seat> seat = template.query(sql, new BeanPropertyRowMapper<Seat>(Seat.class));
        return seat;
    }

    @Override
    public List<Student> findAllStudent(){
        String sql ="select * from student";
        List<Student> student = template.query(sql, new BeanPropertyRowMapper<Student>(Student.class));
        return student;
    }

    @Override
    public List<Distribution> findList() {
        String sql = "select * from distribution where askStatus = 1 || askStatus = -1 || askStatus = 2";
        List<Distribution> list = template.query(sql, new BeanPropertyRowMapper<Distribution>(Distribution.class));
        System.out.println("空参执行"+list);
        return list;
    }

    @Override
    public List<Distribution> findList(String stu_id) {
        String sql = "select * from distribution where stu_id = ?";
        List<Distribution> list = template.query(sql, new BeanPropertyRowMapper<Distribution>(Distribution.class),stu_id);
        System.out.println("单参执行。"+list);
        return list;
    }

    @Override
    public List<Distribution> findList(String roomId, String seatNumber) {
        String seatId = roomId.concat(seatNumber);
        String sql = "select * from distribution where seatId = ?";
        List<Distribution> list = template.query(sql, new BeanPropertyRowMapper<Distribution>(Distribution.class),seatId);
        System.out.println("双参执行。。"+list);
        return list;
    }

    @Override
    public void updateStudent(String stu_id, String stu_name, String grade, String phoneNumber) {
        System.out.println("updateStudent...");
        String sql = "update student set stu_name = ?,grade = ?,phoneNumber = ? where stu_id = ?";
        template.update(sql, stu_name, grade, phoneNumber, stu_id);
    }

    @Override
    public void addSignIn(String stu_id) {
        System.out.println("addSignIn");
        String sql = "insert into distribution values(null,0,?,null)";
        template.update(sql, stu_id);
    }

    @Override
    public String findDistributionById(String stu_id) {
        try {
            String sql = "select * from distribution where stu_id = ?";
            Distribution distribution = template.queryForObject(sql, new BeanPropertyRowMapper<Distribution>(Distribution.class), stu_id);
            return distribution.getAskStatus();
        } catch (DataAccessException e) {
            //e.printStackTrace();
            return null;
        }
    }

    @Override
    public String findDistributionSeatById(String stu_id) {
        try {
            String sql = "select * from distribution where stu_id = ?";
            Distribution distribution = template.queryForObject(sql, new BeanPropertyRowMapper<Distribution>(Distribution.class), stu_id);
            return distribution.getSeatId();
        } catch (DataAccessException e) {
            //e.printStackTrace();
            return null;
        }
    }

    @Override
    public void addLeave(String stu_id, String seatId, String timeStart, String timeEnd, String days) {
       String sql = "insert into leavemsg values(?,?,?,?,?,0)";
        template.update(sql, stu_id, seatId, timeStart, timeEnd, days);
    }

    @Override
    public Leave findLeaveById(String stu_id) {
        try {
            String sql = "select * from leavemsg where stu_id = ? ";
            Leave leave = template.queryForObject(sql, new BeanPropertyRowMapper<Leave>(Leave.class), stu_id);
            System.out.println(leave);
            return leave;
        } catch (DataAccessException e) {
            //e.printStackTrace();
            return null;
        }
    }

    @Override
    public String findSeatById(String stu_id) {
        System.out.println("stu_id in dao " + stu_id);
        String sql = "select * from distribution where stu_id = ?";
        Distribution distribution = template.queryForObject(sql, new BeanPropertyRowMapper<Distribution>(Distribution.class), stu_id);
        System.out.println(distribution);
        return distribution.getSeatId();
    }

    @Override
    public void addChangeSeat(String idA, String seatA, String idB, String seatB) {
        System.out.println("changeSeat执行");
        String sql = "insert into changeseat values(?,?,?,?,?)";
        template.update(sql, idA, seatA, idB, seatB, null);
    }

    @Override
    public String findChangeSeatById(String stu_id) {
        try {
            String sql = "select * from changeseat where idA = ?";
            ChangeSeat changeSeat = template.queryForObject(sql, new BeanPropertyRowMapper<ChangeSeat>(ChangeSeat.class), stu_id);
            return changeSeat.getApproval();
        } catch (DataAccessException e) {
            //e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteSeat(String seatId) {
        String sql = "delete from seat where seatId  = ?";
        template.update(sql,seatId);
    }

    @Override
    public void addSeat(String seatId, String roomId, String seatNumber){
        System.out.println("seatId"+seatId);
        System.out.println("roomId"+roomId);
        System.out.println("seatNumber"+seatNumber);
        String sql = "insert into seat values(?,?,?)";
        template.update(sql,seatId,roomId,seatNumber);
    }

    @Override
    public void deleteStudent(String stu_id) {
        String sql = "delete from student where stu_id  = ?";
        template.update(sql,stu_id);
    }

    @Override
    public void addStudent(String stu_id, String stu_name, String gender,String grade,String phoneNumber,String password){
        System.out.println("stu_id"+stu_id);
        System.out.println("stu_name"+stu_name);
        System.out.println("gender"+gender);
        System.out.println("grade"+grade);
        System.out.println("phoneNumber"+phoneNumber);
        System.out.println("password"+password);
        String sql = "insert into student values(?,?,?,?,?,?)";
        template.update(sql,stu_id,stu_name,gender,grade,phoneNumber,password);
    }

    @Override
    public void updStudent(String stu_id, String stu_name, String gender, String grade, String phoneNumber, String password) {
        System.out.println("stu_id"+stu_id);
        System.out.println("stu_name"+stu_name);
        System.out.println("gender"+gender);
        System.out.println("grade"+grade);
        System.out.println("phoneNumber"+phoneNumber);
        System.out.println("password"+password);
        String sql = "update student set stu_name = ?,gender=?,grade = ?,phoneNumber = ?,password=? where stu_id = ?";
        template.update(sql, stu_name, gender,grade, phoneNumber,password, stu_id);
    }

    @Override
    public Student findStudent(String stu_id) {
        String sql = "select * from student where stu_id  = ?";
        Student student = template.queryForObject(sql, new BeanPropertyRowMapper<Student>(Student.class), stu_id);
        System.out.println("findStudent"+student);
        return student;
    }

    @Override
    public void deleteDistri(String stu_id) {
        String sql = "delete from distribution where stu_id  = ?";
        template.update(sql,stu_id);
    }

    @Override
    public Distribution findListForObject(String stu_id) {
        String sql = "select * from distribution where stu_id = ?";
        Distribution distribution = template.queryForObject(sql, new BeanPropertyRowMapper<Distribution>(Distribution.class), stu_id);
        System.out.println("distribution"+distribution);
        return distribution;
    }

    @Override
    public void updDistribution(String askNumber, String stu_id, String seatId, String askStatus) {
        System.out.println("askNumber"+askNumber);
        System.out.println("stu_id"+stu_id);
        System.out.println("seatId"+seatId);
        System.out.println("askStatus"+askStatus);
        String sql = "update distribution set askNumber = ?,seatId = ?,askStatus = ? where stu_id = ?";
        template.update(sql, askNumber, seatId,askStatus,stu_id);
    }

    @Override
    public Teacher findTeacher(String Tea_id) {
        String sql = "select * from teacher where Tea_id  = ?";
        Teacher Teacher = template.queryForObject(sql, new BeanPropertyRowMapper<Teacher>(Teacher.class), Tea_id);
        System.out.println("findTeacher"+Teacher);
        return Teacher;
    }

    @Override
    public void updTeacher(String tea_id, String tea_name, String gender, String grade, String phoneNumber, String password) {
        System.out.println("tea_id"+tea_id);
        System.out.println("tea_name"+tea_name);
        System.out.println("gender"+gender);
        System.out.println("grade"+grade);
        System.out.println("phoneNumber"+phoneNumber);
        System.out.println("password"+password);
        String sql = "update teacher set tea_name = ?,gender = ?,grade = ?,phoneNumber = ?,password = ? where tea_id = ?";
        template.update(sql, tea_name, gender, grade,phoneNumber,password,tea_id);
    }

    @Override
    public List<Student> findTStudent(String grade) {
        String sql = "select * from student where grade = ?";
        List<Student> list = template.query(sql, new BeanPropertyRowMapper<Student>(Student.class),grade);

        return list;

    }

    @Override
    public List<Leave> findLeav() {
        String sql = "select * from leavemsg where approval = 1";
        List<Leave> les = template.query(sql, new BeanPropertyRowMapper<Leave>(Leave.class));
        System.out.println("空参执行"+les);
        return les;
    }

    @Override
    public List<Leave> findLeav(String stu_id) {
        String sql = "select * from leavemsg where stu_id = ? && approval = 1";
        List<Leave> les = template.query(sql, new BeanPropertyRowMapper<Leave>(Leave.class),stu_id);
        System.out.println("单参执行。"+les);
        return les;
    }

    @Override
    public List<Leave> findLeavRyseat(String seatId) {
        String sql = "select * from leavemsg where seatId = ? && approval = 1";
        List<Leave> les = template.query(sql, new BeanPropertyRowMapper<Leave>(Leave.class),seatId);
        System.out.println("单参执行。"+les);
        return les;
    }

    @Override
    public List<Leave> findLeavRytime(String timeStart) {
        String sql = "select * from leavemsg where timeStart = ? && approval = 1";
        List<Leave> les = template.query(sql, new BeanPropertyRowMapper<Leave>(Leave.class),timeStart);
        System.out.println("单参执行。"+les);
        return les;
    }

    @Override
    public List<Leave> findLeavRytimeE(String timeEnd) {
        String sql = "select * from leavemsg where timeEnd = ? && approval = 1";
        List<Leave> les = template.query(sql, new BeanPropertyRowMapper<Leave>(Leave.class),timeEnd);
        System.out.println("单参执行。"+les);
        return les;
    }

    @Override
    public List<Student> findTStudent(String grade, String stu_id) {
        String sql = "select * from student where grade = ? && stu_id = ?";
        List<Student> list = template.query(sql, new BeanPropertyRowMapper<Student>(Student.class),grade,stu_id);

        return list;
    }

    @Override
    public List<Student> findTStuByname(String grade, String stu_name) {
        String sql = "select * from student where grade = ? && stu_name = ?";
        List<Student> list = template.query(sql, new BeanPropertyRowMapper<Student>(Student.class),grade,stu_name);

        return list;
    }

    @Override
    public List<Seat> findroom(String roomId) {
        String sql = "select * from seat where roomId = ?";
        List<Seat> seat = template.query(sql, new BeanPropertyRowMapper<Seat>(Seat.class),roomId);
        return seat;
    }

    @Override
    public List<Student> findStuId(String stu_id) {
        String sql = "select * from student where stu_id = ?";
        List<Student> list = template.query(sql, new BeanPropertyRowMapper<Student>(Student.class),stu_id);

        return list;
    }

    @Override
    public List<Student> findStugrade(String grade) {
        String sql = "select * from student where grade = ?";
        List<Student> list = template.query(sql, new BeanPropertyRowMapper<Student>(Student.class),grade);

        return list;
    }

    @Override
    public List<ChangeSeat> findAllChanges() {
        String sql = "select * from changeseat";
        List<ChangeSeat> chags = template.query(sql, new BeanPropertyRowMapper<ChangeSeat>(ChangeSeat.class));
        return chags;
    }

    @Override
    public ChangeSeat findChagForObject(String idA) {
        String sql = "select * from changeseat where idA = ?";
        ChangeSeat chags = template.queryForObject(sql,new BeanPropertyRowMapper<ChangeSeat>(ChangeSeat.class),idA);
        System.out.println("changeseat"+chags);
        return chags;
    }

    @Override
    public void updChags(String idA, String seatA, String idB, String seatB, String approval) {
        System.out.println("idA"+idA);
        System.out.println("seatA"+seatA);
        System.out.println("idB"+idB);
        System.out.println("seatB"+seatB);
        System.out.println("approval"+approval);
        String sql = "update changeseat set approval = ? where idA = ?";
        template.update(sql,approval,idA);
    }


    @Override
    public void deleteChag(String idA) {
        String sql = "delete from changeseat where idA  = ?";
        template.update(sql,idA);
    }

    @Override
    public boolean updTeache(String tea_id, String tea_name, String gender, String grade, String phoneNumber, String password) {
        System.out.println("tea_id"+tea_id);
        System.out.println("tea_name"+tea_name);
        System.out.println("gender"+gender);
        System.out.println("grade"+grade);
        System.out.println("phoneNumber"+phoneNumber);
        System.out.println("password"+password);
        String sql = "update teacher set tea_name = ?,gender = ?,grade = ?,phoneNumber = ?,password = ? where tea_id = ?";

        Integer update = template.update(sql, tea_name, gender, grade,phoneNumber,password,tea_id);
        if(update>0){
            return true;
        }
        return false;
    }

    @Override
    public List<Distribution> findlist(Object grade) {
        String sql = "select * from distribution inner JOIN student on distribution.stu_id=student.stu_id && student.grade = ?";
        List<Distribution> list = template.query(sql, new BeanPropertyRowMapper<Distribution>(Distribution.class),(Student.class),grade);
        System.out.println("空参执行"+list);
        return list;

    }

    @Override
    public void deletLeav(String stu_id) {
        String sql = "delete from leavemsg where stu_id  = ?";
        template.update(sql,stu_id);
    }

    @Override
    public void updLev(String approval, String stu_id) {
        String sql = "update leavemsg set approval = ? where stu_id = ?";
        template.update(sql, approval, stu_id);
    }

    @Override
    public List<Distribution> findTList() {
        String sql = "select * from distribution ";
        List<Distribution> list = template.query(sql, new BeanPropertyRowMapper<Distribution>(Distribution.class));
        System.out.println("空参执行"+list);
        return list;
    }

    @Override
    public List<Distribution> findSList() {
        String sql = "select * from distribution";
        List<Distribution> list = template.query(sql, new BeanPropertyRowMapper<Distribution>(Distribution.class));
        System.out.println("空参执行"+list);
        return list;
    }

    @Override
    public List<Distribution> findSListBy(String stu_id) {
        String sql = "select * from distribution where stu_id = ? ";
        List<Distribution> list = template.query(sql, new BeanPropertyRowMapper<Distribution>(Distribution.class),stu_id);
        System.out.println("单参执行。"+list);
        return list;
    }

    @Override
    public Leave findLea(String stu_id) {
        String sql = "select * from leavemsg where stu_id  = ?";
        Leave les = template.queryForObject(sql, new BeanPropertyRowMapper<Leave>(Leave.class), stu_id);
        System.out.println("findLeave"+les);
        return les;
    }

    @Override
    public List<Leave> findTLeav() {
        String sql = "select * from leavemsg ";
        List<Leave> les = template.query(sql, new BeanPropertyRowMapper<Leave>(Leave.class));
        System.out.println("空参执行"+les);
        return les;
    }
    @Override
    public List<Leave> findTLeav(String stu_id) {
        String sql = "select * from leavemsg where stu_id = ? ";
        List<Leave> les = template.query(sql, new BeanPropertyRowMapper<Leave>(Leave.class),stu_id);
        System.out.println("单参执行。"+les);
        return les;
    }

    @Override
    public List<Leave> findTLeavRyseat(String seatId) {
        String sql = "select * from leavemsg where seatId = ? ";
        List<Leave> les = template.query(sql, new BeanPropertyRowMapper<Leave>(Leave.class),seatId);
        System.out.println("单参执行。"+les);
        return les;
    }

    @Override
    public List<Leave> findTLeavRytime(String timeStart) {
        String sql = "select * from leavemsg where timeStart = ? ";
        List<Leave> les = template.query(sql, new BeanPropertyRowMapper<Leave>(Leave.class),timeStart);
        System.out.println("单参执行。"+les);
        return les;
    }

    @Override
    public List<Leave> findTLeavRytimeE(String timeEnd) {
        String sql = "select * from leavemsg where timeEnd = ? ";
        List<Leave> les = template.query(sql, new BeanPropertyRowMapper<Leave>(Leave.class),timeEnd);
        System.out.println("单参执行。"+les);
        return les;
    }


}
