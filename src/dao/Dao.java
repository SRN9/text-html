package dao;

import domain.*;

import java.util.List;

public interface Dao {
    Student stuLogin(Student stu);

    Teacher teaLogin(Teacher tea);

    Manager magLogin(Manager mag);

    List<Seat> findAllRoom();

    List<Student> findAllStudent();

    List<Distribution> findList();

    List<Distribution> findList(String stu_id);

    List<Distribution> findList(String roomId,String seatNumber);

    void updateStudent(String stu_id, String stu_name, String grade, String phoneNumber);

    void addSignIn(String stu_id);

    String findDistributionById(String stu_id);

    String findDistributionSeatById(String stu_id);

    void addLeave(String stu_id, String seatId, String timeStart, String timeEnd, String days);

    Leave findLeaveById(String stu_id);

    String findSeatById(String stu_id);

    void addChangeSeat(String idA, String seatA, String idB, String seatB);

    String findChangeSeatById(String stu_id);

    void deleteSeat(String seatId);


    void addSeat(String seatId, String roomId, String seatNumber);

    void deleteStudent(String stu_id);

    void addStudent(String stu_id, String stu_name, String gender, String grade, String phoneNumber, String password);

    void updStudent(String stu_id, String stu_name, String gender, String grade, String phoneNumber, String password);

    Student findStudent(String stu_id);

    void deleteDistri(String stu_id);


    Distribution findListForObject(String stu_id);

    void updDistribution(String askNumber, String stu_id, String seatId, String askStatus);

    Teacher findTeacher(String Tea_id);

    void updTeacher(String tea_id, String tea_name, String gender, String grade, String phoneNumber, String password);

    List<Student> findTStudent(String grade);

    List<Leave> findLeav();

    List<Leave> findLeav(String stu_id);

    List<Leave> findLeavRyseat(String seatId);

    List<Leave> findLeavRytime(String timeStart);

    List<Leave> findLeavRytimeE(String timeEnd);

    List<Student> findTStudent(String grade, String stu_id);

    List<Student> findTStuByname(String grade, String stu_name);

    List<Seat> findroom(String roomId);

    List<Student> findStuId(String stu_id);

    List<Student> findStugrade(String grade);

    List<ChangeSeat> findAllChanges();

    ChangeSeat findChagForObject(String idA);

    void updChags(String idA, String seatA, String idB, String seatB, String approval);

    void deleteChag(String idA);

    boolean updTeache(String tea_id, String tea_name, String gender, String grade, String phoneNumber, String password);


    List<Distribution> findlist(Object grade);

    void deletLeav(String stu_id);

    void updLev(String approval, String stu_id);


    List<Distribution> findTList();

    List<Distribution> findSList();

    List<Distribution> findSListBy(String stu_id);

    Leave findLea(String stu_id);

    List<Leave> findTLeav();

    List<Leave> findTLeav(String stu_id);

    List<Leave> findTLeavByseat(String seatId);

    List<Leave> findTLeavBytime(String timeStart);

    List<Leave> findTLeavBytimeE(String timeEnd);
}
