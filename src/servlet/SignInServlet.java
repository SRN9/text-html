package servlet;

import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/signInServlet")
public class SignInServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        HttpSession session = request.getSession();
        Object stuId = session.getAttribute("stuId");
        System.out.println(stuId);
        String stu_name = request.getParameter("stu_name");
        String grade = request.getParameter("grade");
        String phoneNumber = request.getParameter("phoneNumber");
        Service service = new ServiceImp();
        String s = service.addSignIn((String) stuId, stu_name, grade, phoneNumber);
        request.setAttribute("msg",s);
        request.getRequestDispatcher("/addSignIn.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
