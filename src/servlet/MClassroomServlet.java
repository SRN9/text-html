package servlet;

import domain.Seat;
import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/mClassroomServlet")
public class MClassroomServlet extends HttpServlet{
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String roomId = request.getParameter("roomId");
        if (roomId == null) roomId = "";

        Service service = new ServiceImp();
        List <Seat> seats;

        if (roomId.length() != 0) {
            seats = service.findroom((String) roomId);
        }else{
            seats = service.findAllRoom();
        }

        request.setAttribute("seats", seats);
        request.getRequestDispatcher("/MClassroom.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
