package servlet;

import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/updateDistriServlet")
public class UpdateDistriServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String askNumber = request.getParameter("askNumber");
        String stu_id = request.getParameter("stu_id");
        String seatId = request.getParameter("seatId");
        String askStatus = request.getParameter("askStatus");


        Service service = new ServiceImp();
        service.updDistribution(askNumber,stu_id,seatId,askStatus);
        request.getRequestDispatcher("/mDistriServlet").forward(request,response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
