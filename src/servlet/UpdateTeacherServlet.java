package servlet;

import domain.Teacher;
import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet("/updateTeacherServlet")
public class UpdateTeacherServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");


        String tea_id = request.getParameter("tea_id");
        String tea_name = request.getParameter("tea_name");
        String gender = request.getParameter("gender");
        String grade = request.getParameter("grade");
        String phoneNumber = request.getParameter("phoneNumber");
        String password = request.getParameter("password");
        System.out.println(tea_id+tea_name+gender+grade+phoneNumber+password);


        Service service = new ServiceImp();
        boolean result = false;

        try{
            result = service.updTeache(tea_id,tea_name,gender,grade,phoneNumber,password);
        }catch (Exception e){
            e.printStackTrace();
        }


        PrintWriter out =response.getWriter();

       if(result){
            out.write("<script>\n"+"alert('修改成功,请重新登陆！');"+"window.history.go(-1);\n"+"</script>");
       }else{
           out.write("<script>\n"+"alert('发生意外');"+"window.history.go(-1);\n"+"</script>");
       }

    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
