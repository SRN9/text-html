package servlet;

import domain.Manager;
import domain.Student;
import domain.Teacher;
import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/loginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String id = request.getParameter("id");
        String password = request.getParameter("password");
        String authority = request.getParameter("authority");
        System.out.println(authority);

        if (authority.equals("1")) {

            Student stu = new Student();
            stu.setStu_id(id);
            stu.setPassword(password);
            Service service = new ServiceImp();
            Student LoginStudent = service.stuLogin(stu);
            if (LoginStudent != null) {
                HttpSession session = request.getSession();
                session.setAttribute("stuName", LoginStudent.getStu_name());
                session.setAttribute("stuId", LoginStudent.getStu_id());
                response.sendRedirect("stu.jsp");
            } else {
                request.setAttribute("login_msg", "用户名或密码错误!");
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            }

        } else if (authority.equals("2")) {

            Teacher tea = new Teacher();
            tea.setTea_id(id);
            tea.setPassword(password);
            Service service = new ServiceImp();
            Teacher LoginTeacher = service.teaLogin(tea);
            if (LoginTeacher != null) {
                HttpSession session = request.getSession();
                session.setAttribute("teaName", LoginTeacher.getTea_name());
                session.setAttribute("teaId", LoginTeacher.getTea_id());
                session.setAttribute("gender", LoginTeacher.getGender());
                session.setAttribute("grade", LoginTeacher.getGrade());
                session.setAttribute("phoneNumber", LoginTeacher.getPhoneNumber());
                session.setAttribute("password", LoginTeacher.getPassword());
                response.sendRedirect("tea.jsp");
            } else {
                request.setAttribute("login_msg", "用户名或密码错误!");
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            }
        } else {
            Manager mag = new Manager();
            mag.setMag_id(id);
            mag.setPassword(password);
            Service service = new ServiceImp();
            Manager LoginManager = service.managerLogin(mag);
            if (LoginManager != null) {
                HttpSession session = request.getSession();
                session.setAttribute("magName", LoginManager.getMag_name());
                session.setAttribute("magId", LoginManager.getMag_id());
                response.sendRedirect("mag.jsp");
            } else {
                request.setAttribute("login_msg", "用户名或密码错误!");
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            }

        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

}
