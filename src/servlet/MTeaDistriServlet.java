package servlet;

import domain.Distribution;
import domain.Student;
import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/mTeaDistriServlet")
public class MTeaDistriServlet extends HttpServlet{
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Object Grade = session.getAttribute("grade");
        System.out.println("MMMMMMM==============="+Grade);

        String stu_id = request.getParameter("stu_id");
        if (stu_id == null) stu_id = "";


        Service service = new ServiceImp();
        List<Distribution> lists;

        if (stu_id.length() != 0) {
            lists = service.findList(stu_id);

        } else {
            lists = service.findTList();
        }

        request.setAttribute("lists", lists);
        request.getRequestDispatcher("/MTeaDistri.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
