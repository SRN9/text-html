package servlet;

import domain.Leave;
import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/leaveResultServlet")
public class LeaveResultServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Object stuId = session.getAttribute("stuId");
        System.out.println("leaveResultServlet "+stuId);

        Service service = new ServiceImp();
        Leave leave = service.leaveResult((String)stuId);
        request.setAttribute("leave", leave);
        request.getRequestDispatcher("/addLeave.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
