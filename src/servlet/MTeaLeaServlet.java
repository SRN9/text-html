package servlet;

import domain.Leave;
import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/mTeaLeaServlet")
public class MTeaLeaServlet extends HttpServlet{
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String stu_id = request.getParameter("stu_id");
        String seatId = request.getParameter("seatId");
        String timeStart = request.getParameter("timeStart");
        String timeEnd = request.getParameter("timeEnd");
        if (stu_id == null) stu_id = "";
        if (seatId == null) seatId = "";
        if (timeStart == null) timeStart = "";
        if (timeEnd == null) timeEnd = "";

        Service service = new ServiceImp();
        List<Leave> les;

        if (stu_id.length() != 0) {
            les = service.findTLeav(stu_id);
        } else if (seatId.length() != 0 ) {
            les = service.findTLeavByseat(seatId);
        } else if (timeStart.length() != 0 ) {
            les = service.findTLeavBytime(timeStart);
        }else if (timeEnd.length() != 0 ) {
            les = service.findTLeavBytimeE(timeEnd);
        }else {
            les = service.findTLeav();
        }
        request.setAttribute("les", les);
        request.getRequestDispatcher("/MTLea.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}


