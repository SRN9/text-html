package servlet;

import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/updateStudentServlet")
public class UpdateStudentServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String stu_id = request.getParameter("stu_id");
        String stu_name = request.getParameter("stu_name");
        String gender = request.getParameter("gender");
        String grade = request.getParameter("grade");
        String phoneNumber = request.getParameter("phoneNumber");
        String password = request.getParameter("password");
        Service service = new ServiceImp();
        service.updStudent(stu_id,stu_name,gender,grade,phoneNumber,password);
        request.getRequestDispatcher("/mStudentServlet").forward(request,response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
