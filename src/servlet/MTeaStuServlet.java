package servlet;

import domain.Student;
import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/mTeaStuServlet")
public class MTeaStuServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        HttpSession session = request.getSession();
        Object Grade = session.getAttribute("grade");
        System.out.println("grade==============="+Grade);

        String stu_id = request.getParameter("stu_id");
        String stu_name = request.getParameter("stu_name");
        if (stu_id == null) stu_id = "";
        if (stu_name == null) stu_name = "";
        Service service = new ServiceImp();

        List<Student> TStudents;
        if (stu_id.length() != 0) {
            TStudents = service.findTStudent((String) Grade,stu_id);
        } else if(stu_name.length() != 0){
            TStudents = service.findTStuByname((String) Grade,stu_name);
        }else{
            TStudents = service.findTStudent((String) Grade);
        }

        request.setAttribute("TStu", TStudents);
        request.getRequestDispatcher("/MTStu.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
