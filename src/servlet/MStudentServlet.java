package servlet;

import domain.Student;
import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/mStudentServlet")
public class MStudentServlet extends HttpServlet{
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String stu_id = request.getParameter("stu_id");
        String grade = request.getParameter("grade");
        if (stu_id == null) stu_id = "";
        if (grade == null) grade = "";

        Service service = new ServiceImp();
        List<Student> students;

        if (stu_id.length() != 0) {
            students = service.findStuId(stu_id);
        } else if (grade.length() != 0) {
            students = service.findStugrade(grade);
        } else {
            students = service.findAllStudent();
        }

        request.setAttribute("students", students);
        request.getRequestDispatcher("/MStudent.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
