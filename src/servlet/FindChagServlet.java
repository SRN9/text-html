package servlet;

import domain.ChangeSeat;
import domain.Distribution;
import domain.Student;
import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/findChagServlet")
public class FindChagServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String idA = request.getParameter("idA");
        System.out.println("findChagServlet"+idA);

        Service service = new ServiceImp();
        ChangeSeat list = service.findChagForObject(idA);
        request.setAttribute("list", list);
        request.getRequestDispatcher("/MUpdChag.jsp").forward(request,response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
