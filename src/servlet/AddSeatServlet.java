package servlet;

import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/addSeatServlet")
public class AddSeatServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String seatId = request.getParameter("seatId");
        String roomId = request.getParameter("roomId");
        String seatNumber = request.getParameter("seatNumber");
        Service service = new ServiceImp();
        service.addSeat(seatId, roomId,seatNumber);
        request.getRequestDispatcher("/mClassroomServlet").forward(request,response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
