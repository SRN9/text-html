package servlet;

import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/changeSeatServlet")
public class ChangeSeatServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String seatId = request.getParameter("seatId");
        String id = request.getParameter("id");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();
        Object stuId = session.getAttribute("stuId");
        System.out.println("changSeatServlet"+stuId);
        Service service = new ServiceImp();
            int i = service.changeSeat((String) stuId ,seatId, id, password);
            if(i == 0){
                request.setAttribute("msg","用户名或密码错误");
                System.out.println("用户名或密码错误");
            }else if(i == 1){
                request.setAttribute("msg","您还未拥有座位或座位填写出错");
                System.out.println("您还未拥有座位或座位填写出错");
            }else if(i == 2){
                request.setAttribute("msg","换座申请已提交，等待审核");
                System.out.println("换座申请已提交，等待审核");
            }else if(i == 3){
                request.setAttribute("msg","审核通过，请及时更换位置");
            }else if(i == 4){
                request.setAttribute("msg","审核未通过，请重试");
            }
            else{
                request.setAttribute("msg","换座需双方同意，提交申请提供对方的账号密码");
            }
        request.getRequestDispatcher("/changeSeat.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
