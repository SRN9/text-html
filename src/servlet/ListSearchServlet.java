package servlet;

import domain.Distribution;
import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/listSearchServlet")
public class ListSearchServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String stu_id = request.getParameter("stu_id");
        String roomId = request.getParameter("roomId");
        String seatNumber = request.getParameter("seatNumber");
        if (stu_id == null) stu_id = "";
        if (roomId == null) roomId = "";
        if (seatNumber == null) seatNumber = "";

        Service service = new ServiceImp();
        List<Distribution> list;

        if (stu_id.length() != 0) {
            list = service.findSListBy(stu_id);
        } else if (roomId.length() != 0 && seatNumber.length() != 0) {
            list = service.findList(roomId, seatNumber);
        } else {
            list = service.findSList();
        }
        request.setAttribute("list", list);
        request.getRequestDispatcher("/listSearch.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
