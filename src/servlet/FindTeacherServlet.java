package servlet;

import domain.Teacher;
import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet("/findTeacherServlet")
public class FindTeacherServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String Tea_id = request.getParameter("Tea_id");
        System.out.println("grade========================"+Tea_id);

        Service service = new ServiceImp();
        Teacher Teacher = service.findTeacher(Tea_id);


        request.setAttribute("Tea", Teacher);
        request.getRequestDispatcher("/MTea.jsp").forward(request,response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
