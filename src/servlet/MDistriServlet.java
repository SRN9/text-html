package servlet;

import domain.Distribution;
import domain.Student;
import service.Service;
import service.serviceImp.ServiceImp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/mDistriServlet")
public class MDistriServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String stu_id = request.getParameter("stu_id");
        String roomId = request.getParameter("roomId");
        String seatNumber = request.getParameter("seatNumber");
        if (stu_id == null) stu_id = "";
        if (roomId == null) roomId = "";
        if (seatNumber == null) seatNumber = "";

        Service service = new ServiceImp();
        List<Distribution> lists;

        if (stu_id.length() != 0) {
            lists = service.findList(stu_id);
        } else if (roomId.length() != 0 && seatNumber.length() != 0) {
            lists = service.findList(roomId, seatNumber);
        } else {
            lists = service.findList();
        }

        request.setAttribute("lists", lists);
        request.getRequestDispatcher("/MDistribution.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
