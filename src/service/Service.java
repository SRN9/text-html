package service;

import domain.*;

import java.util.List;

public interface Service {
    Student stuLogin(Student stu);

    List<Seat> findAllRoom();

    List<Student> findAllStudent();

    List<Distribution> findList();

    List<Distribution> findList(String stu_id);

    List<Distribution> findList(String roomId, String seatNumber);

    String addSignIn(String stu_id, String stu_name, String grade, String phoneNumber);

    void askForLeave(String stu_id, String seatId, String timeStart, String timeEnd, String days);

    Leave leaveResult(String stuId);

    int changeSeat(String stuId, String seatId, String name, String password);

    Teacher teaLogin(Teacher tea);

    Manager managerLogin(Manager mag);

    void deleteSeat(String seatId);

    void addSeat(String seatId, String roomId, String seatNumber);

    void deleteStudent(String stu_id);

    void addStudent(String stu_id, String stu_name, String gender, String grade, String phoneNumber, String password);

    void updStudent(String stu_id, String stu_name, String gender, String grade, String phoneNumber, String password);

    Student findStudent(String stu_id);

    void deleteDistri(String stu_id);


    Distribution findListForObject(String stu_id);

    void updDistribution(String stu_id, String seatId, String askNumber, String askStatus);

    Teacher findTeacher(String Tea_id);

    Teacher updTeacher(String tea_id, String tea_name, String gender, String grade, String phoneNumber, String password);

    List<Student> findTStudent(String Grade);

    List<Leave> findLeav();

    List<Leave> findLeav(String stu_id);

    List<Leave> findLeavByseat(String seatId);

    List<Leave> findLeavBytime(String timeStart);

    List<Leave> findLeavBytimeE(String timeEnd);

    List<Student> findTStudent(String grade, String stu_id);

    List<Student> findTStuByname(String grade, String stu_name);

    List<Seat> findroom(String roomId);

    List<Student> findStuId(String stu_id);

    List<Student> findStugrade(String grade);

    List<ChangeSeat> FindAllChangs();

    ChangeSeat findChagForObject(String idA);

    void updChags(String idA, String seatA, String idB, String seatB, String approval);

    void deleteChag(String idA);

    boolean updTeache(String tea_id, String tea_name, String gender, String grade, String phoneNumber, String password);


    List<Distribution> findList(Object grade);

    void deleteLeav(String stu_id);

    void updLev(String approval, String stu_id);


    List<Distribution> findTList();

    List<Distribution> findSList();

    List<Distribution> findSListBy(String stu_id);

    Leave findLea(String stu_id);

    List<Leave> findTLeav();

    List<Leave> findTLeav(String stu_id);

    List<Leave> findTLeavByseat(String seatId);

    List<Leave> findTLeavBytime(String timeStart);

    List<Leave> findTLeavBytimeE(String timeEnd);
}


