package service.serviceImp;

import dao.Dao;
import dao.daoImp.DaoImp;
import domain.*;
import service.Service;

import java.util.List;

public class ServiceImp implements Service {
    Dao dao = new DaoImp() {
    };

    @Override
    public Student stuLogin(Student stu) {
        return dao.stuLogin(stu);
    }

    @Override
    public List<Seat> findAllRoom() {
        return dao.findAllRoom();
    }

    @Override
    public List<Student> findAllStudent(){return dao.findAllStudent();}


    @Override
    public List<Distribution> findList() {
        return dao.findList();
    }

    @Override
    public List<Distribution> findList(String stu_id) {
        return dao.findList(stu_id);
    }

    @Override
    public List<Distribution> findList(String roomId, String seatNumber) {
        return dao.findList(roomId, seatNumber);
    }

    @Override
    public String addSignIn(String stu_id, String stu_name, String grade, String phoneNumber) {
        if (dao.findDistributionById(stu_id) == null && stu_name == null && grade == null && phoneNumber == null) {
            return "输入信息后立刻开始报名";
        } else if (dao.findDistributionById(stu_id) == null && stu_name != null) {
            dao.updateStudent(stu_id, stu_name, grade, phoneNumber);
            dao.addSignIn(stu_id);
            return "提交成功，等待审核！";
        } else if (dao.findDistributionById(stu_id).equals("0")) {
            return "请勿重复提交，请等待审核！";
        } else if (dao.findDistributionById(stu_id).equals("-1")) {
            return "审核未通过，请重试！";
        } else if (dao.findDistributionById(stu_id).equals("1")) {
            return "审核未通过，请重试！";
        }else{
            return "审核通过，您的座位号为" + dao.findDistributionSeatById(stu_id);
        }
    }

    @Override
    public void askForLeave(String stu_id, String seatId, String timeStart, String timeEnd, String days) {
        dao.addLeave(stu_id, seatId, timeStart, timeEnd, days);
    }

    @Override
    public Leave leaveResult(String stuId) {
        return dao.findLeaveById(stuId);
    }

    @Override
    public int changeSeat(String stuId, String seatId, String id, String password) {
        String cs = dao.findChangeSeatById(stuId);
        if (cs == null && seatId == null && id == null && password == null) {
            return -1;
        } else if (cs == null && seatId != null) {
            Student stu = new Student();
            stu.setStu_id(id);
            stu.setPassword(password);
            Student loginStu = dao.stuLogin(stu);
            String seatA = dao.findSeatById(stuId);
            if (loginStu == null) {
                return 0;
            } else if (dao.findSeatById(loginStu.getStu_id()) == null || seatA == null || !dao.findSeatById(loginStu.getStu_id()).equalsIgnoreCase(seatId)) {
                return 1;
            } else {
                dao.addChangeSeat(stuId, seatA, loginStu.getStu_id(), dao.findSeatById(loginStu.getStu_id()));
                return 2;
            }

        } else if (cs.equals("1")) {
            return 3;
        } else {
            return 4;
        }

    }

    @Override
    public Teacher teaLogin(Teacher tea) {
        return dao.teaLogin(tea);
    }

    @Override
    public Manager managerLogin(Manager mag) { return dao.magLogin(mag); }

    @Override
    public void deleteSeat(String seatId) {
        dao.deleteSeat(seatId);
    }

    @Override
    public void addSeat(String seatId, String roomId, String seatNumber) {dao.addSeat(seatId,roomId,seatNumber);}

    @Override
    public void deleteStudent(String stu_id) {
        dao.deleteStudent(stu_id);
    }


    @Override
    public void addStudent(String stu_id, String stu_name, String gender,String grade,String phoneNumber,String password)
    {dao.addStudent(stu_id,stu_name,gender,grade,phoneNumber,password);}

    @Override
    public void updStudent(String stu_id, String stu_name, String gender,String grade,String phoneNumber,String password)
    {dao.updStudent(stu_id,stu_name,gender,grade,phoneNumber,password);}

    @Override
    public Student findStudent(String stu_id) {
        return dao.findStudent(stu_id);
    }

    @Override
    public void deleteDistri(String stu_id) {
        dao.deleteDistri(stu_id);
    }

    @Override
    public Distribution findListForObject(String stu_id) {
        return dao.findListForObject(stu_id);
    }

    @Override
    public void updDistribution(String askNumber,String stu_id, String seatId,  String askStatus) {
        dao.updDistribution(askNumber,stu_id,seatId,askStatus);
    }

    @Override
    public Teacher findTeacher(String Tea_id) {
        return dao.findTeacher(Tea_id);
    }

    @Override
    public Teacher updTeacher(String tea_id, String tea_name, String gender, String grade, String phoneNumber, String password) {
        dao.updTeacher(tea_id,tea_name,gender,grade,phoneNumber,password);
        return null;
    }

    @Override
    public List<Student> findTStudent(String Grade) {

        return dao.findTStudent(Grade);
    }

    @Override
    public List<Leave> findLeav() {
        return dao.findLeav();
    }

    @Override
    public List<Leave> findLeav(String stu_id) {
        return dao.findLeav(stu_id);
    }

    @Override
    public List<Leave> findLeavByseat(String seatId) {
        return dao.findLeavRyseat(seatId);
    }

    @Override
    public List<Leave> findLeavBytime(String timeStart) {
        return dao.findLeavRytime(timeStart);
    }

    @Override
    public List<Leave> findLeavBytimeE(String timeEnd) {
        return dao.findLeavRytimeE(timeEnd);
    }

    @Override
    public List<Student> findTStudent(String grade, String stu_id) {
        return dao.findTStudent(grade,stu_id);
    }

    @Override
    public List<Student> findTStuByname(String grade, String stu_name) {
        return dao.findTStuByname(grade,stu_name);
    }

    @Override
    public List<Seat> findroom(String roomId) {
        return dao.findroom(roomId);
    }

    @Override
    public List<Student> findStuId(String stu_id) {
        return dao.findStuId(stu_id);
    }

    @Override
    public List<Student> findStugrade(String grade) {
        return dao.findStugrade(grade);
    }

    @Override
    public List<ChangeSeat> FindAllChangs() {
        return dao.findAllChanges();
    }

    @Override
    public ChangeSeat findChagForObject(String idA) {
         return dao.findChagForObject(idA);
    }

    @Override
    public void updChags(String idA, String seatA, String idB, String seatB, String approval) {
        dao.updChags(idA,seatA,idB,seatB,approval);
    }

    @Override
    public void deleteChag(String idA) {
        dao.deleteChag(idA);
    }

    @Override
    public boolean updTeache(String tea_id, String tea_name, String gender, String grade, String phoneNumber, String password) {
        return dao.updTeache(tea_id,tea_name,gender,grade,phoneNumber,password);
    }

    @Override
    public List<Distribution> findList(Object grade) {
        return dao.findlist(grade);
    }

    @Override
    public void deleteLeav(String stu_id) {
        dao.deletLeav(stu_id);
    }

    @Override
    public void updLev(String approval, String stu_id) {
        dao.updLev(approval,stu_id);
    }

    @Override
    public List<Distribution> findTList() {
        return dao.findTList();
    }

    @Override
    public List<Distribution> findSList() {
        return dao.findSList();
    }

    @Override
    public List<Distribution> findSListBy(String stu_id) {
        return dao.findSListBy(stu_id);
    }

    @Override
    public Leave findLea(String stu_id) {
        return dao.findLea(stu_id);
    }

    @Override
    public List<Leave> findTLeav() {
        return dao.findTLeav();
    }

    @Override
    public List<Leave> findTLeav(String stu_id) {
        return dao.findTLeav(stu_id);
    }

    @Override
    public List<Leave> findTLeavByseat(String seatId) {
        return dao.findTLeavByseat(seatId);
    }

    @Override
    public List<Leave> findTLeavBytime(String timeStart) {
        return dao.findTLeavBytime(timeStart);
    }

    @Override
    public List<Leave> findTLeavBytimeE(String timeEnd) {
        return dao.findTLeavBytimeE(timeEnd);
    }


}



