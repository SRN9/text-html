package domain;

public class Leave {
    private String stu_id;
    private String seatId;
    private String timeStart;
    private String timeEnd;
    private String days;
    private int approval;

    public String getStu_id() {
        return stu_id;
    }

    public void setStu_id(String stu_id) {
        this.stu_id = stu_id;
    }

    public String getSeatId() {
        return seatId;
    }

    public void setSeatId(String seatId) {
        this.seatId = seatId;
    }

    public String getTimeStart() { return timeStart; }

    public void setTimeStart(String timeStart) { this.timeStart = timeStart; }

    public String getTimeEnd() { return timeEnd; }

    public void setTimeEnd(String timeEnd) { this.timeEnd = timeEnd; }

    public String getDays() { return days; }

    public void setDays(String days) { this.days = days; }

    public int getApproval() { return approval; }

    public void setApproval(int approval) { this.approval = approval; }

    @Override
    public String toString() {
        return "Leave{" +
                "stu_id='" + stu_id + '\'' +
                ", seatId='" + seatId + '\'' +
                ", timeStart='" + timeStart + '\'' +
                ", timeEnd='" + timeEnd + '\'' +
                ", days='" + days + '\'' +
                ", approval=" + approval +
                '}';
    }
}
