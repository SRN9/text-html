package domain;

public class ChangeSeat {
    private String seatA;
    private String idA;
    private String seatB;
    private String idB;
    private String approval;

    public String getSeatA() {
        return seatA;
    }

    public void setSeatA(String seatA) {
        this.seatA = seatA;
    }

    public String getIdA() {
        return idA;
    }

    public void setIdA(String idA) {
        this.idA = idA;
    }

    public String getSeatB() {
        return seatB;
    }

    public void setSeatB(String seatB) {
        this.seatB = seatB;
    }

    public String getIdB() {
        return idB;
    }

    public void setIdB(String idB) {
        this.idB = idB;
    }

    public String getApproval() {
        return approval;
    }

    public void setApproval(String approval) {
        this.approval = approval;
    }

    @Override
    public String toString() {
        return "ChangeSeat{" +
                "seatA='" + seatA + '\'' +
                ", idA='" + idA + '\'' +
                ", seatB='" + seatB + '\'' +
                ", idB='" + idB + '\'' +
                ", approval='" + approval + '\'' +
                '}';
    }
}
