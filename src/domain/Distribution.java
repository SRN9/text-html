package domain;

public class Distribution {
    private int askNumber;
    private String askStatus;
    private String stu_id;
    private String seatId;

    public int getAskNumber() {
        return askNumber;
    }

    public void setAskNumber(int askNumber) {
        this.askNumber = askNumber;
    }

    public String getAskStatus() {
        return askStatus;
    }

    public void setAskStatus(String askStatus) {
        this.askStatus = askStatus;
    }

    public String getStu_id() {
        return stu_id;
    }

    public void setStu_id(String stu_id) {
        this.stu_id = stu_id;
    }

    public String getSeatId() {
        return seatId;
    }

    public void setSeatId(String seatId) {
        this.seatId = seatId;
    }

    @Override
    public String toString() {
        return "Distribution{" +
                "askNumber=" + askNumber +
                ", askStatus='" + askStatus + '\'' +
                ", stu_id='" + stu_id + '\'' +
                ", seatId='" + seatId + '\'' +
                '}';
    }
}
