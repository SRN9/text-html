package domain;

public class Teacher {
    private String Tea_id;
    private String Tea_name;
    private String password;
    private String gender;
    private String grade;
    private String phoneNumber;

    public String getTea_id() {
        return Tea_id;
    }

    public void setTea_id(String Tea_id) {
        this.Tea_id = Tea_id;
    }

    public String getTea_name() {
        return Tea_name;
    }

    public void setTea_name(String Tea_name) {
        this.Tea_name = Tea_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "Tea_id='" + Tea_id + '\'' +
                ", Tea_name='" + Tea_name + '\'' +
                ", password='" + password + '\'' +
                ", gender='" + gender + '\'' +
                ", grade='" + grade + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}

