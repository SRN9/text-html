package domain;

public class Manager {
    private String mag_id;
    private String mag_name;
    private String password;

    public String getMag_id() {
        return mag_id;
    }

    public void setMag_id(String mag_id) {
        this.mag_id = mag_id;
    }

    public String getMag_name() {
        return mag_name;
    }

    public void setMag_name(String mag_name) {
        this.mag_name = mag_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Manager{" +
                "mag_id='" + mag_id + '\'' +
                ", mag_name='" + mag_name + '\'' +
                ", password='" + password +
                '}';
    }
}